﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipInput : MonoBehaviour {

    // Creamos siempre una clase input para que sea la única que acceda a los inputs. Encapsularlo en una única clase. Vamos, utilizat siempre el input para todo. 
    // Le da la información del eje.

    private Vector2 axis;
    public Weapons weapons;
    public PlayerBehaviour player;

    // Update is called once per frame
    void Update() {

        axis.x = Input.GetAxis("Horizontal");
        axis.y = Input.GetAxis("Vertical");

        player.SetAxis(axis);

        if(Input.GetButton("Fire1")) {
            weapons.ShootWeapons();
        }
        if(Input.GetKeyDown(KeyCode.Z)) //hemos detectado cómo el usuario cambiaa la z.
        {
            weapons.NextWeapon();

        }

    }
}
