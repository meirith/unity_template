﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{

    public float couldown;
    private float timeCounter;
    private bool canShoot;

    //Create bullets
    public GameObject bullet;
    public int maxBullets;
    public Transform bulletPosition;

    private Cartridge cart;
    private Vector3 pos1, pos2;

    void Start()
    {
        canShoot = true;
        timeCounter = 0;
        cart = new Cartridge(bullet, maxBullets, bulletPosition);
    }

    void Update()
    {
        if(!canShoot)
        {
            timeCounter += Time.deltaTime;
            if(timeCounter >= couldown)
            {
                canShoot = true;
            }
        }
    }

    public void ShootCannon(Bullet bullet, Vector3 position, float rotation) {

        if(canShoot)
            canShoot = false;
            timeCounter = 0;
        bullet.Shot(position, rotation);
    }
}